#!/bin/bash  
  
CROSSPREFIX=arm-hisiv100-linux-uclibcgnueabi-
AR="${CROSSPREFIX}ar"  
RANLIB="${CROSSPREFIX}ranlib"  
CC="${CROSSPREFIX}gcc"  
  
libtool --tag=CC --mode=compile ${CROSSPREFIX}gcc -g -O2 -MT linux-atomic.lo -MD -MP -MF linux-atomic.Tpo -c -o linux-atomic.lo linux-atomic.c  
libtool --tag=CC --mode=link ${CROSSPREFIX}gcc -module -avoid-version -g -O2 -o liblinux-atomic.la linux-atomic.lo 
